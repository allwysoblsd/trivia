
export const fetchQuestions = () =>
  fetch("https://opentdb.com/api.php?amount=10&category=9&difficulty=easy&type=multiple").then(
    (response) => response.json()
  );
