import { createStore, applyMiddleware } from 'redux';
import mainReducer from '../reducer';
import sagaMiddleware from "../saga/initial";
import rootSaga from "../saga/root";

const store = createStore(mainReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

export default store
