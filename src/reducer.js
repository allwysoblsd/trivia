import { FETCH_QUESTIONS_PENDING, FETCH_QUESTIONS_SUCCESS } from './action';

const initialState = {
    questions: [],
    isFetchQuestionsComplete: ''
}

function mainReducer(state=initialState, action) {
    switch (action.type) {
        case FETCH_QUESTIONS_PENDING :
            return { 
                ...state, 
                isFetchQuestionsComplete: 'PENDING'
            };
            case FETCH_QUESTIONS_SUCCESS :
            return { 
                ...state, 
                questions: action.data, 
                isFetchQuestionsComplete: 'SUCCESS'
            }
        default:
            return state;
    }
}

export default mainReducer;
