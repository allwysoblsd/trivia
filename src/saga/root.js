import { all, takeEvery } from "redux-saga/effects";
import { FETCH_QUESTIONS_PENDING } from "../action";
import { getQuestions } from "./saga";
export default function* root() {
    yield all([yield takeEvery(FETCH_QUESTIONS_PENDING, getQuestions)]);
}
