import { put } from "redux-saga/effects";
import { FETCH_QUESTIONS_SUCCESS } from "../action";
import { fetchQuestions } from "../config/request";

export function* getQuestions() {
  const questions = yield fetchQuestions();
  yield put({ type: FETCH_QUESTIONS_SUCCESS, data: questions });
}
