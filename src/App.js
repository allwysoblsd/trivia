import React, { Component } from 'react';
import { connect } from "react-redux";
import _ from 'lodash';
import './App.css';
import { loadData } from './action';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOption: ""
    }
    this.handleOptionChange = this.handleOptionChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    this.props.loadData();
  }

  handleOptionChange(event) {
    this.setState({
      selectedOption: event.target.value
    });
  }
 
  handleSubmit(event) {
    event.preventDefault();
    const checked = _.find(this.props.questions.results, ['correct_answer', this.state.selectedOption])
    if(checked === undefined) {
      alert('Your choice is Wrong')
    } else if(checked.correct_answer === this.state.selectedOption) {
      alert('You made your Choice as ' + this.state.selectedOption + ' and is correct')
    } else{
        alert('try again')
    }
  }

  render() {
    if(this.props.status === 'SUCCESS') {
    }
    return (
      <div className="App">
        <header className="App-header">
          Trivia Quiz
          </header>
        {this.props.status === 'SUCCESS' ?
          <body>
            {this.props.questions.results.map((questList) => {
              return (
                <div>
                  <form onSubmit={this.handleSubmit}>
                    <ol>{questList.question}</ol>
                    <div className="options">
                      <label>
                        <input type="radio" value={questList.correct_answer} checked ={this.state.selectedOption === questList.correct_answer} onChange={this.handleOptionChange} />
                        {questList.correct_answer}
                      </label>
                      {questList.incorrect_answers.length ? questList.incorrect_answers.map((options) => {
                        return (
                            <div className="radio">
                              <label>
                                <input type="radio" value={options} checked ={this.state.selectedOption === options} onChange={this.handleOptionChange} />
                                {options}
                              </label>
                            </div>
                        )
                      }) : "Oops, Try again!"}
                    </div>
                    <button type="submit">Check</button>
                  </form>
                </div>
              )
            })}
          </body>
          : "Loading..."}
      </div>
    );
  }
}


const mapStateToProps = (state) => ({
  status: state.isFetchQuestionsComplete,
  questions: state.questions,
});

const mapDispatchToProps = (dispatch) => ({
  loadData: () => dispatch(loadData()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
